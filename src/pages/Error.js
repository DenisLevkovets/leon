import React from 'react';
import logo from '../images/IMC_logo.png'
import inst from '../images/socialIcons/Instagram_icon.svg'
import vk from '../images/socialIcons/Vk_icon.svg'
import status from '../images/status.svg'
import * as styles from './Error.module.scss'
import axios from 'axios'

class ErrorPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: true
        }
    }

    animation(e) {
        e.preventDefault();

        setTimeout(
            function () {
                const email = document.getElementById('emailInput').value;
                let bodyFormData = new FormData();
                bodyFormData.append('email', email);
                const url = `https://imctime.com/emails/`;
                axios.post(url, bodyFormData);
            }
            , 3000);

        if (this.state.animation) {
            document.getElementById('submitButton').innerHTML = '';
            document.getElementById('submitButton').className = styles.animation;
            document.getElementById('emailInput').className = styles.animation2;
            setTimeout(() => document.getElementById('submitButton').innerHTML = 'УСПЕШНО.', 3000);
            this.setState({animation: false})
        }
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.content}>
                    <img className={styles.logo} src={logo} alt='Logo'/>
                    <img className={styles.errorStatus} src={status} alt={'404'}/>
                    <form className={styles.emailForm} onSubmit={e => this.animation(e)}>
                        <input className={styles.emailInput} id='emailInput' type='email' placeholder='Введите e-mail'
                               required/>
                        <button className={styles.submitButton} id='submitButton' type='submit'>ОТПРАВИТЬ</button>
                    </form>
                </div>
                <div className={styles.footer}>
                    <p className={styles.firstHeader}>Сайт скоро снова появится.</p>

                    <p className={styles.secondHeader}>Подпишитесь на нашу e-mail рассылку, чтобы первыми получать все
                        новости, узнавать о скидках и получать подарки.</p>
                    <div className={styles.socialIcons}>
                        <a href='https://www.instagram.com/imcwatches/' target="_blank" rel="noopener noreferrer">
                            <img src={inst} alt='Instagram'/>
                        </a>
                        <a href='https://vk.com/imcwatches' target="_blank" rel="noopener noreferrer">
                            <img className={styles.vk} src={vk} alt='VKontakte'/>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorPage;