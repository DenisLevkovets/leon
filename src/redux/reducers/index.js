import {combineReducers} from 'redux';
import {watch} from './watchesReducer';
import {color} from './colorReducer';



const rootReducer = combineReducers({
  watch,
  color
});

export default rootReducer;
