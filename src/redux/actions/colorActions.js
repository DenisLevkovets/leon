import constants from '../constants';

function setColor(color) {
    return{
        type: constants.currentColor.SET_COLOR,
        color: color
    }
}


export default {
    setColor
}