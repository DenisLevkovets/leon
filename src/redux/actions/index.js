import watchesActions from './watchesActions';
import colorActions from './colorActions';

export default {
  watchesActions,
  colorActions
}