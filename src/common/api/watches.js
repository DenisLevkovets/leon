import photo from '../../images/watches/mirageBlack.png'

const mock = [
    {
        id: 1,
        model: 'Mirage',
        mainColor:'Silver',
        color1: 'Silver',
        color2: 'Black',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 15000,
        blockColor: '#84d7e7'
    },
    {
        id: 2,
        model: 'Mirage',
        mainColor:'Silver/Black',
        color1: 'Silver/Black',
        color2: 'Black',
        braceletType: 'Metal',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 15000,
        blockColor: '#ee8335'

    },
    {
        id: 3,
        model: 'Mirage',
        mainColor:'Black',
        color1: 'Black',
        color2: 'Black',
        braceletType: 'Metal',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 15000,
        blockColor: '#3d3f3c'

    },
    {
        id: 4,
        model: 'Mondiale',
        mainColor:'Gold',
        color1: 'Gold',
        color2: 'Gold',
        braceletType: 'Metal',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 17000,
        blockColor: '#fdd757'

    },
    {
        id: 5,
        model: 'Mondiale',
        mainColor:'Gold/Leather',
        color1: 'Gold',
        color2: 'Gold',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 17000,
        blockColor: '#fdd757'

    },
    {
        id: 6,
        model: 'Mondiale',
        mainColor:'Silver',
        color1: 'Silver',
        color2: 'Gold',
        braceletType: 'Metal',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 17000,
        blockColor: '#f6f6f6'
    },
    {
        id: 7,
        model: 'Mondiale',
        mainColor:'Silver/Leather',
        color1: 'Silver',
        color2: 'Gold',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#f6f6f6'

    },
    {
        id: 8,
        model: 'Iconic',
        mainColor:'Blue',
        color1: 'Silver',
        color2: 'Blue',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#021f59'
    },
    {
        id: 9,
        model: 'Iconic',
        mainColor:'Ivory',
        color1: 'Silver',
        color2: 'Ivory',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#e2d0b6'
    },
    {
        id: 10,
        model: 'Iconic',
        mainColor:'Aqua',
        color1: 'Silver',
        color2: 'Aqua',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#049dd9'
    },
    {
        id: 11,
        model: 'Iconic',
        mainColor:'Grey',
        color1: 'Silver',
        color2: 'Grey',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#a6958f'
    },
    {
        id: 12,
        model: 'Iconic',
        mainColor:'Brown',
        color1: 'Silver',
        color2: 'Brown',
        braceletType: 'Leather',
        photo: {
            main: photo,
            close: photo,
            case: photo,
            carousel: [photo, photo, photo],
            bottom: photo
        },
        price: 18000,
        blockColor: '#592d1d'
    },
];

function getWatchesList() {

    const data = new Promise((resolve) => {
        resolve(mock)
    });
    return data;
}

function getWatchModel(model) {
    let filter = mock.filter(item => item.model === model);
    const data = new Promise((resolve) => {
        resolve(filter)
    });
    return data;
}

export default {
    getWatchesList,
    getWatchModel
}