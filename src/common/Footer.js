import React from 'react';
import * as styles from './Footer.module.scss'
import logo from "../images/IMC_logo.png";
import instagram from "../images/socialIcons/Instagram_dark_icon.svg";
import vk from "../images/socialIcons/Vk_dark_icon.svg";


class Footer extends React.Component {
    render() {
        return (
            <div className={styles.footer}>
                <div className={styles.footerContent}>
                    <img className={styles.logo} src={logo} alt='Logo'/>
                    <div className={styles.contacts}>
                        <p className={styles.title}>КОНТАКТЫ</p>
                        <p className={styles.description}>
                            +7 812 449 78 80<br/>
                            contact@imclogo.com<br/>
                            Найти магазины
                        </p>
                    </div>

                    <div className={styles.help}>
                        <p className={styles.title}>ПОМОЩЬ</p>
                        <p className={styles.description}>
                            Покупка и оплата<br/>
                            Доставка<br/>
                            Возврат<br/>
                            FAQ<br/>
                            Регулировка ремешка
                        </p>
                    </div>

                    <div className={styles.info}>
                        <p className={styles.title}>ИНФОРМАЦИЯ</p>
                        <p className={styles.description}>
                            Политика конфиденциальности<br/>
                            Политика возврата
                        </p>
                    </div>

                    <div className={styles.rightPanel}>
                        <div className={styles.socialIcons}>
                            <img className={styles.instagram} src={instagram} alt='instagram'/>
                            <img className={styles.vk} src={vk} alt='vk'/>
                        </div>

                        <p className={styles.watermark}>® IMC Manufactoria 2019</p>
                    </div>
                </div>


            </div>
        )
    }
}

export default Footer;