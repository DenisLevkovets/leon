import React from 'react';
import Error from './pages/Error'


function App() {
  return (
    <div className="App">
        <Error/>
    </div>
  );
}

export default App;
