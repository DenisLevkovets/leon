import React from 'react'
import * as styles from './Men.module.scss'
import chooseColor from '../../common/params'
import {Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Slider from '../../common/slider';


class Men extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color1: '',
            color2: '',
            bracelet: '',
            price: [0, 999999999].slice(),
            listToRender: this.props.list
        };
    }

    color1Sort(color1) {
        if (this.state.color1 === color1) color1 = '';
        const {color2, bracelet, price} = this.state;
        let sortedList = this.props.list.filter(item => (
                (color1.length === 0 ? true : item.color1 === color1) &&
                (color2.length === 0 ? true : item.color2 === color2) &&
                (bracelet.length === 0 ? true : item.braceletType === bracelet) &&
                item.price >= price[0] &&
                item.price <= price[1]
            )
        );
        this.setState({listToRender: sortedList, color1})
    }

    color2Sort(color2) {
        if (this.state.color2 === color2) color2 = '';
        const {color1, bracelet, price} = this.state;
        let sortedList = this.props.list.filter(item => (
                (color1.length === 0 ? true : item.color1 === color1) &&
                (color2.length === 0 ? true : item.color2 === color2) &&
                (bracelet.length === 0 ? true : item.braceletType === bracelet) &&
                item.price >= price[0] &&
                item.price <= price[1]
            )
        );
        this.setState({listToRender: sortedList, color2})
    }

    braceletTypeSort(bracelet) {
        if (this.state.bracelet === bracelet) bracelet = '';
        const {color1, color2, price} = this.state;
        let sortedList = this.props.list.filter(item => (
                (color1.length === 0 ? true : item.color1 === color1) &&
                (color2.length === 0 ? true : item.color2 === color2) &&
                (bracelet.length === 0 ? true : item.braceletType === bracelet) &&
                item.price >= price[0] &&
                item.price <= price[1]
            )
        );
        this.setState({listToRender: sortedList, bracelet})
    }

    onChangePrice = price => {
        const {color1, color2, bracelet} = this.state;
        let sortedList = this.props.list.filter(item => (
                (color1.length === 0 ? true : item.color1 === color1) &&
                (color2.length === 0 ? true : item.color2 === color2) &&
                (bracelet.length === 0 ? true : item.braceletType === bracelet) &&
                item.price >= price[0] &&
                item.price <= price[1]
            )
        );
        this.setState({listToRender: sortedList})
    };


    onUpdatePrice = price => {
        this.setState({price: price})
    };


    getData() {
        const color1 = [];
        const color2 = [];
        const braceletType = [];
        const price = [];
        this.props.list.forEach(item => {
            if (!color1.includes(item.color1)) color1.push(item.color1);
            if (!color2.includes(item.color2)) color2.push(item.color2);
            if (!braceletType.includes(item.braceletType)) braceletType.push(item.braceletType);
            if (!price.includes(item.price)) price.push(item.price)
        });
        return [color1, color2, braceletType, price.sort()];
    }

    render() {
        const [color1, color2, braceletType, price] = this.getData();
        return (
            <div className={styles.men}>
                <div className={styles.header}>
                    <div style={{paddingLeft: '350px', width: '100%'}}>
                        <h1 className={styles.title}>МУЖСКИЕ ЧАСЫ</h1>
                        <h3 className={styles.description}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/>
                            sed diam nonumy eirmod tempor invidunt ut labore et</h3>
                    </div>
                </div>

                <div className={styles.content}>
                    <Row className={styles.row}>
                        <Col className={styles.leftPanel}>
                            <h1 className={styles.filtersTitle}>Фильтры</h1>
                            <div className={styles.divider}/>
                            <h3 className={styles.filtersParam}>Цвет корпуса:</h3>
                            <div className={styles.paramValues}>
                                {color1.map((color, index) => (
                                    <div className={styles.imageDiv} key={color}>
                                        <img
                                            className={this.state.color1 === color ? [styles.selectedColor, styles.color].join(' ') : styles.color}
                                            src={chooseColor.chooseColor(color)} alt={color}
                                            onClick={() => this.color1Sort(color1[index])}/>
                                    </div>
                                ))}
                            </div>
                            <div className={styles.divider}/>
                            <h3 className={styles.filtersParam}>Цвет циферблата:</h3>
                            <div className={styles.paramValues}>
                                {color2.map((color, index) => (
                                    <div className={styles.imageDiv} key={color}>
                                        <img
                                            className={this.state.color2 === color ? [styles.selectedColor, styles.color].join(' ') : styles.color}
                                            src={chooseColor.chooseColor(color)} alt={color}
                                            onClick={() => this.color2Sort(color2[index])}/>
                                    </div>
                                ))}
                            </div>
                            <div className={styles.divider}/>
                            <h3 className={styles.filtersParam}>Тип браслета:</h3>
                            <div className={[styles.paramValues, styles.braceletValues].join(' ')}>
                                {braceletType.map((type, index) => (
                                    <p className={this.state.bracelet === type ? styles.selectedBracelet : styles.notSelectedBracelet}
                                       key={type} onClick={() => this.braceletTypeSort(braceletType[index])}>
                                        {chooseColor.chooseBracelet(type)}
                                    </p>
                                ))}
                            </div>
                            <div className={styles.divider}/>
                            <h3 className={styles.filtersParam}>Цена:</h3>
                            <Slider onUpdate={this.onUpdatePrice} onChange={this.onChangePrice}
                                    values={this.state.price}
                                    max={price[price.length - 1]}/>
                            <div className={styles.range}>
                                <p>от <span
                                    className={styles.rangeValue}>{this.state.price[0].toLocaleString().replace(/,/g, ' ')}</span>
                                </p>
                                <p>до <span
                                    className={styles.rangeValue}>{this.state.price[1].toLocaleString().replace(/,/g, ' ')}</span>
                                </p>
                            </div>

                        </Col>
                        <Col className={styles.watches}>
                            {this.state.listToRender.map((item) => {
                                return (
                                    <div key={item.id} className={styles.watchItem}>
                                        <Link to={'/watch/' + item.model} onClick={() => {
                                            this.props.setColor(item.mainColor);
                                        }}>
                                            <img className={styles.watch} src={item.photo.main} alt={'watch'}/>

                                        </Link>
                                        <div className={styles.watchDescription}>
                                            <p>{item.model.toLowerCase()} </p>
                                            <p>{item.mainColor.toLowerCase()}</p>
                                        </div>
                                    </div>
                                )
                            })}
                        </Col>
                    </Row>
                </div>

            </div>
        )
    }
}


export default Men;