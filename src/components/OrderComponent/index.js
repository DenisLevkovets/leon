import Order from './Order/Order'
import Delivery from './Delivery/Delivery'
import Payment from './Payment/Payment'
import Done from './Done/Done'

export default {
    Order,
    Delivery,
    Payment,
    Done
}
