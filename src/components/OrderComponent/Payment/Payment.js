import React from 'react'
import * as styles from './Payment.module.scss'
import status from "../../../images/statusPayment.png";
import {Col, Form, Row} from "react-bootstrap";
import arrowBack from "../../../images/Arrow_left.svg";
import arrowNext from "../../../images/Arrow_right.svg";
import visa from "../../../images/Visa_icon.svg";
import card from "../../../images/Card_icon.svg";
import paypal from "../../../images/Paypal_icon.png";
import {Link} from 'react-router-dom'


class Payment extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = {
            validated: false,
            selectedMethod: 0,
            cardNum: '',
            inspireDate: '',
        };
    }

    handleChange(e, field) {
        const value = e.target.value;
        switch (field) {
            case 'cardNum':
                value.replace(/ /g, '').length % 4 === 0 && value.replace(/ /g, '').length !== 16 ? this.setState({cardNum: value + '   '}) : this.setState({cardNum: value});
                break;
            case 'inspireDate':
                value.length === 2 ? this.setState({inspireDate: value + ' / '}) : this.setState({inspireDate: value});
                break;
        }
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            this.setState({validated: true});
        } else {
            event.preventDefault();

            let data = {
                number: form.cardNum.value,
                owner: form.owner.value,
                inspireDate: form.inspireDate.value,
                secureCode: form.secureCode.value
            };
            localStorage.setItem('card', JSON.stringify(data));
            const cart = JSON.parse(localStorage.getItem('cart'));
            const price = localStorage.getItem('total');
            let orders = JSON.parse(localStorage.getItem('orders'));
            if (orders) {
                orders.splice(0, 0, {order: cart, price: price});
                localStorage.setItem('orders', JSON.stringify(orders))
            } else localStorage.setItem('orders', JSON.stringify([{order: cart, price: price}]));
            localStorage.removeItem('cart');
            localStorage.removeItem('total');
            this.setState({validated: true});
            this.props.history.push('/order/done')
        }
    }

    render() {
        const address = JSON.parse(localStorage.getItem('address'));
        const cart = JSON.parse(localStorage.getItem('cart'));
        const totalPrice = JSON.parse(localStorage.getItem('total'));

        return (
            <div className={styles.payment}>
                <div className={styles.header}>
                    <div style={{paddingLeft: '373px', width: '100%'}}>
                        <h1 className={styles.title}>ОПЛАТА</h1>
                        <h3 className={styles.description}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/>
                            sed diam nonumy eirmod tempor invidunt ut labore et</h3>
                    </div>
                    <img src={status} alt={'status'} className={styles.status}/>
                </div>

                <Row className={styles.content}>
                    <Col className={styles.form}>
                        <p className={styles.title}>Выберите способ оплаты</p>

                        <div className={styles.paymentMethod}>
                            <button
                                className={this.state.selectedMethod === 0 ? [styles.paymentType, styles.focus].join(' ') : styles.paymentType}
                                onClick={() => this.setState({selectedMethod: 0})}>
                                <img src={card} alt={'card'}/>
                            </button>
                            <button
                                className={this.state.selectedMethod === 1 ? [styles.paymentType, styles.focus].join(' ') : styles.paymentType}
                                onClick={() => this.setState({selectedMethod: 1})}>
                                <img src={paypal} alt={'paypal'}/>
                            </button>
                        </div>

                        <h1 className={styles.cardInfo}>Информация о кредитной карте</h1>
                        <Form className={styles.formItem}
                              noValidate
                              validated={this.state.validated}
                              onSubmit={e => this.handleSubmit(e)}
                              id="myform">

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="cardNum" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>НОМЕР КАРТЫ</Form.Label>
                                    <Row style={{margin: '0'}}>
                                        <Form.Control
                                            required
                                            className={styles.formInput}
                                            type="text"
                                            name="cardNum"
                                            onChange={(e) => this.handleChange(e, 'cardNum')}
                                            value={this.state.cardNum}
                                            maxLength={'25'}
                                        />

                                        <div className={styles.visa}>
                                            <img src={visa} alt={'visa'}/>
                                        </div>
                                        <Form.Control.Feedback type="invalid">Введите НОМЕР</Form.Control.Feedback>
                                    </Row>
                                </Form.Group>


                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId='owner' className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ВЛАДЕЛЕЦ КАРТЫ</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInputFull}
                                        type="text"
                                        name="owner"
                                        defaultValue={'LEON RUBNER'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите ИМЯ</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="inspireDate" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ДАТА ОКОНЧАНИЯ СРОКА ДЕЙСТВИЯ</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInputFull}
                                        type="text"
                                        name="inspireDate"
                                        onChange={(e) => this.handleChange(e, 'inspireDate')}
                                        value={this.state.inspireDate}
                                        maxLength={'7'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите СРОК</Form.Control.Feedback>
                                </Form.Group>

                            </Form.Row>


                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="secureCode" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>КОД БЕЗОПАСНОСТИ</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInputFull}
                                        type="text"
                                        name="secureCode"
                                        defaultValue={'123'}
                                        maxLength={'3'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите КОД</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>
                        </Form>

                    </Col>

                    <Col className={styles.order}>
                        <div className={styles.address}>
                            <h1 className={styles.addressHeader}>Адрес доставки</h1>
                            <div className={styles.addressData}>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.name}</p>
                                    <p className={styles.addressField}>{address.surname}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressFieldFull}>{address.email}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.country}</p>
                                    <p className={styles.addressField}>{address.city}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.index}</p>
                                    <p className={styles.addressField}>{address.phone}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressFieldFull}>{address.address}</p>
                                </Row>
                            </div>
                        </div>
                        {cart && cart.map(item => (
                            <div className={styles.item} key={item.model.id}>
                                <img src={item.model.photo.main} alt={'watch'} className={styles.watch}/>
                                <div className={styles.modelDescription}>
                                    <p className={styles.model}>{item.model.model}</p>
                                    <p className={styles.modelSex}>Мужские часы</p>
                                </div>
                                <p className={styles.price}>{(item.model.price * item.count).toLocaleString().replace(/,/g, ' ')} RUB</p>
                            </div>
                        ))

                        }


                        <div className={styles.total}>
                            <p className={styles.totalText}>Всего</p>
                            <p className={styles.totalPrice}>{(+totalPrice).toLocaleString().replace(/,/g, ' ')} RUB</p>
                        </div>
                    </Col>
                </Row>

                <div className={styles.btnRow}>
                    <Link to={'/order/delivery'}>
                        <button className={styles.back}>
                            <img className={styles.arrowBack} src={arrowBack} alt={'back'}/>
                            <span className={styles.backText}>НАЗАД</span>
                        </button>
                    </Link>

                    <button className={styles.next} type="submit" form="myform">
                        <span className={styles.nextText}>ОПЛАТИТЬ</span>
                        <img className={styles.arrowNext} src={arrowNext} alt={'next'}/>
                    </button>
                </div>
            </div>
        )
    }
}

export default Payment;