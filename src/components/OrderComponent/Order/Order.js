import React from 'react';
import * as styles from './Order.module.scss'
import {connect} from 'react-redux'
import status from '../../../images/status.png'
import close from '../../../images/Delete_button.svg'
import question from '../../../images/question.png'
import arrowBack from '../../../images/Arrow_left.svg'
import arrowNext from '../../../images/Arrow_right.svg'
import minus from '../../../images/minus.svg'
import plus from '../../../images/plus.svg'


import {Link} from 'react-router-dom'
import actions from "../../../redux/actions";


class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHovering: false,
            cart: JSON.parse(localStorage.getItem('cart')),
            totalPrice: localStorage.getItem('total')
        };
    }

    componentDidMount() {
        this.props.getWatchesList();
    }

    handleMouseHover() {
        this.setState({isHovering: !this.state.isHovering});
    }

    handleColorChange(e, item) {
        const cart = this.state.cart;
        const index = cart.indexOf(item);
        const itemToReplace = cart.splice(index, 1)[0];
        const color = itemToReplace.colors.splice(itemToReplace.colors.indexOf(e.target.value), 1)[0];
        itemToReplace.colors.splice(0, 0, color);

        const itemForReplace = {
            model: this.props.list.find(element => element.model === item.model.model && element.color1 === e.target.value),
            count: itemToReplace.count,
            colors: itemToReplace.colors
        };
        const merge = cart.find(element => element.model.id === itemForReplace.model.id);
        if (merge) {
            const mergeIndex = cart.findIndex(element => element.model.id === merge.model.id)
            cart[mergeIndex].count += itemForReplace.count
        } else {
            cart.splice(index, 0, itemForReplace);
        }
        localStorage.setItem('cart', JSON.stringify(cart));
        this.setState({cart})
    }

    plusItem(item) {
        let {cart, totalPrice} = this.state;

        let index = cart.indexOf(item);
        cart[index].count += 1;
        totalPrice = +totalPrice + cart[index].model.price;
        localStorage.setItem('cart', JSON.stringify(cart));
        localStorage.setItem('total', totalPrice);
        this.setState({cart, totalPrice})
    }

    minusItem(item) {
        let {cart, totalPrice} = this.state;

        let index = cart.indexOf(item);
        cart[index].count -= 1;
        totalPrice = +totalPrice - cart[index].model.price;
        localStorage.setItem('cart', JSON.stringify(cart));
        localStorage.setItem('total', totalPrice);
        this.setState({cart, totalPrice})
    }


    removeItem(item) {
        let {cart, totalPrice} = this.state;

        let index = cart.indexOf(item);
        totalPrice -= cart[index].count * cart[index].model.price;
        cart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(cart));
        localStorage.setItem('total', totalPrice);
        this.setState({cart, totalPrice})
    }

    render() {
        const {cart, totalPrice} = this.state;
        return (
            <div className={styles.order}>
                <div className={styles.header}>
                    <div style={{paddingLeft: '373px', width: '100%'}}>
                        <h1 className={styles.title}>МОЙ ЗАКАЗ</h1>
                        <h3 className={styles.description}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/>
                            sed diam nonumy eirmod tempor invidunt ut labore et</h3>
                    </div>
                    <img src={status} alt={'status'} className={styles.status}/>
                </div>

                <div className={styles.container}>

                    <div className={styles.row}>
                        <div className={styles.orderList}>
                            {cart && cart.map(item => (

                                <div className={styles.item} key={item.model.id}>
                                    <img src={item.model.photo.main} alt={'watch'} className={styles.watch}/>
                                    <div className={styles.modelDescription}>
                                        <p className={styles.model}>{item.model.model}</p>
                                        <p className={styles.modelSex}>Мужские часы</p>
                                    </div>
                                    <div className={styles.color}>
                                        <label>
                                            <select className={styles.selectColor}
                                                    onChange={(e) => this.handleColorChange(e, item)}>

                                                {item.colors.map(color => (
                                                    <option key={color}>{color}</option>
                                                ))}
                                            </select>
                                        </label>
                                    </div>
                                    <div className={styles.quantity}>

                                        <button className={styles.minus} onClick={() => {
                                            if (item.count > 1) this.minusItem(item)
                                        }}><img src={minus} alt={'minus'}/></button>
                                        <input value={item.count} required disabled type='number' min='1' max='100'/>
                                        <button className={styles.plus} onClick={() => this.plusItem(item)}><img
                                            src={plus} alt={'plus'}/></button>
                                    </div>

                                    <p className={styles.price}>{(item.model.price * item.count).toLocaleString().replace(/,/g, ' ')} RUB</p>

                                    <img src={close} className={styles.close} alt={'close'}
                                         onClick={() => this.removeItem(item)}/>
                                </div>
                            ))}
                        </div>
                        <div className={styles.promocodeContainer}>
                            <div className={styles.promocode}>

                                <div className={styles.promocodeText}>
                                    <p>Промокод</p>
                                    <img src={question} alt={'question'}
                                         onMouseEnter={() => this.handleMouseHover()}
                                         onMouseLeave={() => this.handleMouseHover()}/>
                                </div>

                                <input className={styles.promocodeInput} placeholder={'Введите промокод'}/>

                            </div>
                            {this.state.isHovering &&
                            <div className={styles.promocodeHover}>
                                <p className={styles.hoverHint}>Чтобы получить промокод,<br/>
                                    Вы можете подписаться на<br/>
                                    почтовую рассылку</p>
                            </div>
                            }

                        </div>
                    </div>
                    <div className={styles.total}>
                        <p className={styles.totalText}>Всего</p>
                        <p className={styles.totalPrice}>{(+totalPrice).toLocaleString().replace(/,/g, ' ')} RUB</p>
                    </div>
                    <div className={styles.btnRow}>
                        <Link to={'/men'}>
                            <button className={styles.back}>
                                <img className={styles.arrowBack} src={arrowBack} alt={'back'}/>
                                <span className={styles.backText}>НАЗАД</span>
                            </button>
                        </Link>
                        <Link to={'/order/delivery'}>
                            <button className={styles.next}>
                                <span className={styles.nextText}>ДАЛЕЕ</span>
                                <img className={styles.arrowNext} src={arrowNext} alt={'next'}/>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>


        )
    }
}

function mapStateToProps(state) {
    return {
        list: state.watch.list,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getWatchesList: () => dispatch(actions.watchesActions.getWatchesList()),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Order);