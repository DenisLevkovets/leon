import React from 'react';
import * as styles from './Delivery.module.scss'
import status from "../../../images/statusDelivery.png";
import {Row, Col, Form} from 'react-bootstrap'
import arrowBack from "../../../images/Arrow_left.svg";
import arrowNext from "../../../images/Arrow_right.svg";
import {Link} from 'react-router-dom'


class Delivery extends React.Component {
    constructor(...args) {
        super(...args);
        this.state = {
            validated: false,
        };
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            this.setState({validated: true});
        } else {
            event.preventDefault();
            let data = {
                name: form.name.value,
                surname: form.surname.value,
                email: form.email.value,
                country: form.country.value,
                city: form.city.value,
                index: form.index.value,
                phone: form.phone.value,
                address: form.address.value
            };
            localStorage.setItem('address', JSON.stringify(data));
            this.setState({validated: true});
            this.props.history.push('/order/payment/')
        }
    }


    render() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const totalPrice = localStorage.getItem('total');
        return (
            <div className={styles.delivery}>
                <div className={styles.header}>
                    <div style={{paddingLeft: '373px', width: '100%'}}>
                        <h1 className={styles.title}>ДОСТАВКА</h1>
                        <h3 className={styles.description}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/>
                            sed diam nonumy eirmod tempor invidunt ut labore et</h3>
                    </div>
                    <img src={status} alt={'status'} className={styles.status}/>
                </div>
                <Row className={styles.content}>
                    <Col className={styles.form}>
                        <p className={styles.title}>Информация об адресе доставки</p>
                        <p className={styles.description}>Заполните информацию о себе.<br/>
                            Графы "Имя" и "Фамилия" заполните, пожалуйста, латинскими буквами.<br/>
                            Остальные поля можно заполнить как латиницей, так и кириллицей.</p>
                        <Form className={styles.formItem}
                              noValidate
                              validated={this.state.validated}
                              onSubmit={e => this.handleSubmit(e)}
                              id="myform">
                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="name" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ИМЯ*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="name"
                                        defaultValue={'Leon'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите ИМЯ</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} controlId="surname" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ФАМИЛИЯ*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="surname"
                                        defaultValue={'Rubner'}

                                    />
                                    <Form.Control.Feedback type="invalid">Введите ФАМИЛИЮ</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="email" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>E-MAIL*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInputFull}
                                        type="text"
                                        name="email"
                                        defaultValue={'leonrubner2809@gmail.com'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите E-MAIL</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="country" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>СТРАНА*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="country"
                                        defaultValue={'Россия'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите СТРАНУ</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="city" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ГОРОД*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="city"
                                        defaultValue={'Санкт-Петербург'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите ГОРОД</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} controlId="state" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ШТАТ</Form.Label>
                                    <Form.Control
                                        className={styles.formInput}
                                        type="text"
                                        name="state"
                                        defaultValue={'Санкт-Петербург'}
                                    />
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="index" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ПОЧТОВЫЙ ИНДЕКС*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="index"
                                        defaultValue={'197349'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите ИНДЕКС</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} controlId="phone" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>ТЕЛЕФОН*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInput}
                                        type="text"
                                        name="phone"
                                        defaultValue={'+79213010708'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите НОМЕР</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className={styles.formRow}>
                                <Form.Group as={Col} controlId="address" className={styles.formGroup}>
                                    <Form.Label className={styles.formLabel}>АДРЕС*</Form.Label>
                                    <Form.Control
                                        required
                                        className={styles.formInputFull}
                                        type="text"
                                        name="address"
                                        defaultValue={'leonrubner2809@gmail.com'}
                                    />
                                    <Form.Control.Feedback type="invalid">Введите АДРЕС</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>
                        </Form>

                    </Col>

                    <Col className={styles.order}>
                        <div className={styles.orderList}>
                            {cart && cart.map(item => (
                                <div className={styles.item} key={item.model.id}>
                                    <img src={item.model.photo.main} alt={'watch'} className={styles.watch}/>
                                    <div className={styles.modelDescription}>
                                        <p className={styles.model}>{item.model.model}</p>
                                        <p className={styles.modelSex}>Мужские часы</p>
                                    </div>
                                    <p className={styles.price}>{(item.model.price * item.count).toLocaleString().replace(/,/g, ' ')} RUB</p>
                                </div>
                            ))}
                        </div>
                        <div className={styles.total}>
                            <p className={styles.totalText}>Всего</p>
                            <p className={styles.totalPrice}>{(+totalPrice).toLocaleString().replace(/,/g, ' ')} RUB</p>
                        </div>
                    </Col>
                </Row>
                <div className={styles.btnRow}>
                    <Link to={'/order'}>

                        <button className={styles.back}>
                            <img className={styles.arrowBack} src={arrowBack} alt={'back'}/>
                            <span className={styles.backText}>НАЗАД</span>
                        </button>
                    </Link>

                    <button className={styles.next} type="submit" form="myform">
                        <span className={styles.nextText}>ДАЛЕЕ</span>
                        <img className={styles.arrowNext} src={arrowNext} alt={'next'}/>
                    </button>

                </div>
            </div>
        )
    }
}

export default Delivery;