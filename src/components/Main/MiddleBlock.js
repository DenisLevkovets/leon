import React from 'react';
import * as styles from './MiddleBlock.module.scss'
import {Carousel} from 'react-bootstrap'


class MiddleBlock extends React.Component {
    render() {
        const model = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])];
        return (
            <div className={styles.middleBlock}>
                <div className={styles.leftSquare}
                     style={model.mainColor.split('/')[1] === 'Leather' ? {background: 'linear-gradient(to right bottom, ' + model.blockColor + ' 50%, #171717 50%)'} : {backgroundColor: model.blockColor}}/>
                <Carousel className={styles.carousel}
                          indicators={false}
                          prevIcon={<span className={styles.prevIcon}> &lt; </span>}
                          nextIcon={<span className={styles.nextIcon}> > </span>}
                >
                    {model && model.photo.carousel.map((photo, index) => (
                        <Carousel.Item key={index}>
                            <img
                                className="d-block w-100 "
                                style={{width: '759px', height: '521px'}}
                                src={photo}
                                alt={index}
                            />
                        </Carousel.Item>
                    ))}

                </Carousel>
                <div className={styles.rightSquare}/>
            </div>
        )
    }
}

export default MiddleBlock;