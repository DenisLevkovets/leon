import React from 'react';
import * as styles from './EmailForm.module.scss'

class EmailForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: true
        }
    }

    animation(e) {
        e.preventDefault();
        if (this.state.animation) {
            document.getElementById('submitButton').innerHTML = '';
            document.getElementById('submitButton').className = styles.animation;
            document.getElementById('emailInput').className = styles.animation2;
            setTimeout(() => document.getElementById('submitButton').innerHTML = 'УСПЕШНО.', 3000);
            this.setState({animation: false})
        }
    }

    render() {
        const model = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])];

        return (
            <div className={styles.emailForm}>
                <div className={styles.leftSquare}
                     style={model.braceletType === 'Leather' ? {background: 'linear-gradient(to left bottom, ' + model.blockColor + ' 50%, #171717 50%)'} : {backgroundColor: model.blockColor}}/>

                <div className={styles.form}>
                    <div className={styles.formContent}>

                        <p className={styles.title}>БУДЬ ПЕРВЫМ</p>
                        <p className={styles.description}>Получай последние новости</p>
                        <p className={styles.description2}> и предложения на почту</p>
                        <form className={styles.sendEmail}>
                            <input className={styles.emailInput} id='emailInput' type='email'
                                   placeholder='e-mail адрес'/>
                            <button className={styles.submitButton} id='submitButton' type='submit'
                                    onClick={e => this.animation(e)}>ОТПРАВИТЬ
                            </button>
                        </form>
                    </div>
                </div>
                <div className={styles.rightSquare}/>
            </div>
        )
    }
}

export default EmailForm;